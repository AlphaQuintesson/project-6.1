public class Animal {
    String Name;
    int Age;
    String Color;

    public Animal(String Name, int Age, String Color)
    {
        this.Name = Name;
        this.Age = Age;
        this.Color = Color;
    }

    public void printer()
    {
        System.out.printf("Наименование: %s\nВозраст: %d\nЦвет: %s\n", Name, Age, Color);
    }
}