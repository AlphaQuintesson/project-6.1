public class Hamster extends Animal{
    float Hair;

    public Hamster(String Name, int Age, String Color, float Hair)
    {
        super(Name, Age, Color);
        this.Hair = Hair;
        this.Name = Name + "Hamster";
        super.Name = Name + "Animal";
    }

    public void printer()
    {
        System.out.printf("Наименование: %s\nВозраст: %d\nЦвет: %s\nДлина шерсти: %s\n", Name, Age, Color, Hair);
    }
}
