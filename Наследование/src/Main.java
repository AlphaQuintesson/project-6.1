import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Animal anim = new Animal("Животное", 12, "Зелёный");
        anim.printer();
        Turtle turtle = new Turtle("Морская черепаха", 240, "Коричневый", true);
        turtle.printer();
        Hamster hamster = new Hamster("Хомяк", 5, "Палевый", 30);
        hamster.printer();
        Animal animal = new Turtle("Сухопутная черепаха", 285, "Зелёный", false);
        animal.printer();

        List<Animal> animalList = new ArrayList<>();
        animalList.add(new Animal("Животное", 12, "Зелёный"));
        animalList.add(new Turtle("Морская черепаха", 240, "Коричневый", true));
        animalList.add(new Hamster("Хомяк", 5, "Палевый", 30));

        for (Animal b : animalList) {
            if (b instanceof Turtle) {
                System.out.println("Класс Turtle");
            } else if (b instanceof Hamster) {
                System.out.println("Класс Hamster");
            } else {
                System.out.println("Класс Animal");
            }
            b.printer();
        }
    }
}