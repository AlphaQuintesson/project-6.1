public class Turtle extends Animal {
    boolean LiveInWater;

    public Turtle(String Name, int Age, String Color, boolean LiveInWater) {
        super(Name, Age, Color);
        this.LiveInWater = LiveInWater;
    }

    public void printer() {
        System.out.printf("Наименование: %s\nВозраст: %d\nЦвет: %s\nЖивёт в воде или нет: %b\n", Name, Age, Color, LiveInWater);
    }
}