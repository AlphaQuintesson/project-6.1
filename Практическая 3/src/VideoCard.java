public class VideoCard {
    private String proizvoditel;
    private int VideoMemory;
    private float chastota;
    private String name;
    private double cost;

    public VideoCard(String proizvoditel,String name, double cost,int VideoMemory,float chastota){
        this.proizvoditel=proizvoditel;
        this.name=name;
        this.cost=cost;
        this.VideoMemory=VideoMemory;
        this.chastota=chastota;
    }

    public void setProizvoditel(String proizvoditel) {
        this.proizvoditel = proizvoditel;
    }

    public void setVideoMemory(int videoMemory) {
        VideoMemory = videoMemory;
    }

    public void setChastota(float chastota) {
        this.chastota = chastota;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getProizvoditel() {
        return proizvoditel;
    }

    public int getVideoMemory() {
        return VideoMemory;
    }

    public float getChastota() {
        return chastota;
    }

    public String getName() {
        return name;
    }

    public double getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return String.format("Производитель: %s \n Название: %s\n Цена: %f \n Видеопамять: %d\n Частота: %f",proizvoditel,name,cost,VideoMemory,chastota);
    }
}
