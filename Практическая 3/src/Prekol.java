import com.sun.jdi.event.StepEvent;

public class Prekol {
    private String Sort;
    private int Kolvo;
    private String Postavka;
    private String Vid;
    private double Cena;

    public Prekol(String Sort, int Kolvo, String Postavka, String Vid, double Cena){
        this.Sort=Sort;
        this.Vid=Vid;
        this.Postavka=Postavka;
        this.Kolvo=Kolvo;
        this.Cena=Cena;
    }
    public void setSort(String sort){
        this.Sort = sort;
    }
    public void setKolvo(int kolvo){
        this.Kolvo = kolvo;
    }
    public void setPostavka(String postavka){
        this.Postavka = postavka;
    }
    public void setVid(String vid){
        this.Vid = vid;
    }
    public void setCena(int prixod){
        this.Cena = prixod;
    }
    public String getSort(){
        return Sort;
    }
    public int getKolvo(){
        return Kolvo;
    }
    public String getPostavka(){
        return Postavka;
    }
    public String getVid(){
        return Vid;
    }
    public double getCena(){
        return Cena;
    }

    @Override
    public String toString() {
        return String.format("Сорт: %s \n Вид: %s \n Поставщик: %s \n Количество: %d \n Цена: %f",Sort,Vid,Postavka,Kolvo,Cena);
    }
}
