import com.sun.java.accessibility.util.TopLevelWindowListener;

import java.time.Period;
import java.util.Scanner;

public class Java{
    public static void main(String[] args) {
        int dengi = 0, chai, cdaha, cena = 0;
        byte cap, coffee, moloko, cahar, topping, cirop, alhocol, decert;
        System.out.println("Тебе нужно выбрать стаканчик: *1 - Small(8$)| *2 - Medium(10$) | *3 - XL(15$) | *4 - XXl(25$)");
        Scanner caps = new Scanner(System.in);
        cap = caps.nextByte();
        switch (cap){
            case 1: System.out.println("Ты выбрал стаканчик размера Small");cena += 8;break;
            case 2: System.out.println("Ты выбрал стаканчик размера Medium");cena += 10;break;
            case 3: System.out.println("Ты выбрал стаканчик размера XL");cena +=15;break;
            case 4: System.out.println("Ты выбрал стаканчик размера XXL");cena +=25;break;
            default: System.out.println("Видимо, ты ошибся, поэтому тебе стоит снова выбрать стаканчик");break;
        }
        System.out.println("Твой заказ вышел на сумму: "+cena);
        System.out.println("Выбери один из видов нашего кофе: *1 - Латте(20$) | 2* - Экспрессо(15$) | 3* - Капучино(25$) | *4 Пряничный мокачинно(35$)");
        Scanner cofe = new Scanner(System.in);
        coffee = cofe.nextByte();
        switch (coffee){
            case 1: System.out.println("Ты выбрал кофе Латте");cena += 20;break;
            case 2: System.out.println("Ты выбрал кофе Экспрессо");cena += 15;break;
            case 3: System.out.println("Ты выбрал кофе Капучино");cena +=25;break;
            case 4: System.out.println("Ты выбрал кофе Пряничный мокачинно");cena += 35;break;
            default: System.out.println("Видимо, ты ошибся, поэтому тебе стоит снова выбрать кофе");break;
        }
        System.out.println("Твой заказ вышел на сумму: "+cena);
        System.out.println("Выбери объём молока, которое мы должны добавить в твой кофе: *1 - Не добавлять молоко | *2 - 15мг(5$) | *3 - 30мг(7$) | *4 - 100мг(11$)");
        Scanner milk = new Scanner(System.in);
        moloko = milk.nextByte();
        switch (moloko){
            case 1: System.out.println("Ты выбрал не добавлять молоко в кофе");break;
            case 2: System.out.println("Ты выбрал, чтобы мы добавили тебе 15 мг молока в твой кофе");cena +=5;break;
            case 3: System.out.println("Ты выбрал, чтобы мы добавили тебе 30 мг молока в твой кофе");cena +=7;break;
            case 4: System.out.println("Ты выбрал, чтобы мы добавили тебе 100 мг молока в твой кофе");cena +=11;break;
            default: System.out.println("Видимо, ты ошибся, поэтому тебе стоит снова выбрать добавлять молоко в кофе или нет");
        }
        System.out.println("Твой заказ вышел на сумму: "+cena);
        System.out.println("Выбери сколько  кусочков сахара тебе добавлять в кофе: *1 - Не добавлять сахар в кофе | *2 - 3(2$) | *3 - 2(1$) | *4 - 5(3$)");
        Scanner sugar = new Scanner(System.in);
        cahar = sugar.nextByte();
        switch (cahar){
            case 1: System.out.println("Ты выбрал, чтобы мы не добавляли сахар в твой кофе");break;
            case 2: System.out.println("Ты выбрал, чтобы мы добавили тебе 3 кусочка сахара");cena += 2;break;
            case 3: System.out.println("Ты выбрал, чтобы мы добавили тебе 2 кусочка сахара");cena += 1;break;
            case 4: System.out.println("Ты выбрал, чтобы мы добавили тебе 5 кусочков сахара");cena +=3;break;
            default: System.out.println("Видимо, ты ошибся, поэтому тебе придётся снова выбрать добавлять ли сахар в твоё кофе");
        }
        System.out.println("Твой заказ вышел на сумму:"+cena);
        System.out.println("Выбери какой алкогольный напиток добавить в твой кофе: *1 - Не добавлять алкоголь в кофе | *2 - Ликёр(5$) | *3 - Водка(2$) | *4 - Ром(15$)");
        Scanner alkash = new Scanner(System.in);
        alhocol = alkash.nextByte();
        switch (alhocol){
            case 1: System.out.println("Ты выбрал, чтобы мы не добавляли алкоголь в твой кофе (ну и зря!)");break;
            case 2: System.out.println("Ты выбрал, чтобы мы добавили Ликёр в твой кофе");cena += 5;break;
            case 3: System.out.println("Ты выбрал, чтобы мы добавили Водку в твой кофе");cena += 10;break;
            case 4: System.out.println("Ты выбрал, чтобы мы добавили Ром в твой кофе");cena += 15;break;
            default: System.out.println("Подумай ещё раз, день должен быть поистине РАДОСТНЫМ И ЗВОНКИМ");
        }
        System.out.println("Твой заказ вышел на сумму: "+cena);
        System.out.println("Выбери какой топпинг хочешь добавить в свой кофе: *1 - Маршмеллоу(4$) | *2 - Шоколад(5$) | *3 - Сироп(7$) | *4 - Посыпка в стиле Флага Украины(100$) ");
        Scanner top = new Scanner(System.in);
        topping = top.nextByte();
        switch (topping){
            case 1: System.out.println("Ты выбрал, чтобы мы добавили в твой кофе Маршмеллоу");cena += 4;break;
            case 2: System.out.println("Ты выбрал, чтобы мы добавили в твой кофе Шоколад");cena += 5;break;
            case 3: System.out.println("Ты выбрал, чтобы мы добавили в твой кофе Сироп");cena += 7;break;
            case 4: System.out.println("Ты выбрал, чтобы мы добавили в твой кофе Посыпку в стиле Флага Украины");cena += 100;break;
            default: System.out.println("Видимо, ты ошибся, поэтому тебе стоит снова выбрать добавлять топпинг в твой кофе или нет");
        }
        System.out.println("Твой заказ вышел на сумму: "+cena);
        System.out.println("Выбери какой сироп добавлять в твоё кофе: *1 - Не добавлять сироп в кофе | *2 - Шоколадный сироп(6$) | *3 - Карамельный сироп(10$) | *4 - Ванильный сироп(5$)");
        Scanner sirop = new Scanner(System.in);
        cirop = sirop.nextByte();
        switch (cirop){
            case 1: System.out.println("Ты выбрал, чтобы мы не добавляли сироп в твой кофе");break;
            case 2: System.out.println("Ты выбрал, чтобы мы добавили Шоколадный сироп в твой кофе");cena += 6;break;
            case 3: System.out.println("Ты выбрал, чтобы мы добавили Карамельный сироп в твой кофе");cena += 10;break;
            case 4: System.out.println("Ты выбрал, чтобы мы добавили в твой кофе Шоколадный сироп");cena += 5;break;
            default: System.out.println("Видимо, ты ошибся, поэтому тебе стоит снова выбрать добавлять сироп в твой кофе или нет ");
        }
        System.out.println("Твой заказ вышел на сумму: "+cena);
        System.out.println("Выбери какой десерт добавить к твоему кофе: *1 - Не добавлять десерт | *2 - Кекс(8$) | *3 - Мороженое(10$) | *4 - Чизкейк(20$)");
        Scanner desert = new Scanner(System.in);
        decert = desert.nextByte();
        switch (decert){
            case 1: System.out.println("Ты выбрал, чтобы мы не добавляли десерт к твоему кофе");break;
            case 2: System.out.println("Ты выбрал, чтобы мы добавили Кекс к твоему кофе");cena += 8;break;
            case 3: System.out.println("Ты выбрал, чтобы мы добавили Мороженое к твоему кофе");cena += 10;break;
            case 4: System.out.println("Ты выбрал, чтобы мы добавили Чизкейк к твоему кофе");cena += 20;break;
            default: System.out.println("Видимо, ты ошибся, поэтому тебе стоит снова выбрать добавлять десерт к твоему кофе или не");
        }
        System.out.println("Твой заказ вышел на сумму: "+cena);
        {
            int Price = cena;
            Scanner scan =  new Scanner(System.in);
            System.out.println("Вы хотите завершить заказ? (д/н)");
            char choise = scan.next().charAt(0);
            if (choise == 'д' || choise == 'Д'){
                System.out.println("Ваш заказ: " + cena);
                System.out.println("Сумма вашего заказа вышла на: " + (double)Math.round(Price * 100000d)/100000d);
                System.out.println("К оплате: ");
            }
            double ToPay = scan.nextDouble();
            if (ToPay >= Price){
                double Change = ToPay - cena;
                System.out.println("Вот ваша сдача: "+Change);
                System.out.println("Введите сумму чеевых(если не желаете оставлять чаевые - введите 0):");
                double Tip = scan.nextDouble();
                System.out.println("Ваши чаевые:"+Tip);
                if (Tip > 0){
                    System.out.println("Спасибо за оставленные чаевые!");
                    System.out.println("Чаевые:"+Tip);
                }
                else System.out.println("Так не пойдёт. Не хотите оставлять - введите 0!");
                System.out.println("Спасибо за покупку");
            }
            else
            {
                do {
                    System.out.println("Внесите указанную сумму");
                    ToPay = scan.nextDouble();
                }while (ToPay < Price);
                double Change = ToPay - Price;
                System.out.println("Вот ваша сдача"+Change);
                System.out.println("Введите сумму чаевых(если не желаете оставлять чаевые - введите 0");
                double Tip = scan.nextDouble();
                if (Tip > 0){
                    System.out.println("Спасибо за оставленные чаевые");
                    System.out.println("Ваши чаевые"+Tip);
                }
                else System.out.println("Так не пойдёт. Не хотите оставлять - введите 0!");
                System.out.println("Спасибо за покупку");
            }
        }
    }
}


