public class vidCard{
        private String proizvoditel;
        private int videoMemory;
        private float chastota;
        private String name;
        private double cost;

        public vidCard(String proizvoditel, String name, int videoMemory, float chastota, double cost) {
            this.proizvoditel=proizvoditel;
            this.name=name;
            this.cost=cost;
            this.videoMemory=videoMemory;
            this.chastota=chastota;

        }
        public void setproizvoditel(String proizvoditel) {this.proizvoditel=proizvoditel;}
        public void setvideoMemory (int videoMemory) {this.videoMemory=videoMemory;}
        public void setname (String name) {this.name=name;}
        public void setchastota (float chastota) {this.chastota=chastota;}
        public void setcost (double cost) {this.cost=cost;}

        public String getproizvoditel() {return proizvoditel;}
        public int getVideoMemory() {return videoMemory;}
        public String getname() {return name;}
        public float getChastota() {return chastota;}
        public double getcost() {return cost;}
        public String toString () {
            return String.format("Производитель: %s \n Название %s \n Цена %f \n Видеопамять %d \n Частота %f ",proizvoditel,name,cost,videoMemory,chastota);
        }
    }
