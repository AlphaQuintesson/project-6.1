import java.util.Scanner;

    public class Magazine{
        public static void main(String[] args){
            int b = 10;
            int vid =0;
            vidCard[] videoCards= new vidCard[b];
            Boks[] boks = new Boks[b];
            Scanner scanner = new Scanner(System.in);
            int i = 0;
            while (true) {
                System.out.println("С каким товаром вы хотите работать 1 - Видео Карты \n2 - Товар");
                vid = scanner.nextInt();
                if (vid==1)
                {

                    System.out.println("1 - Добавление товара \n 2 - Редактирование товара \n 3 - Просмотр товара");
                    String input = scanner.next();
                    switch (input) {
                        case "1":
                            System.out.println("Добавление товара");
                            videoCards[i++] = new vidCard(inputZnak("Введите производителя"), inputZnak("Введите наименование"), Integer.valueOf(inputZnak("Введите объем видеопамяти")), Float.valueOf(inputZnak("Введите частоту")), Double.valueOf(inputZnak("Введите цену")));
                            break;
                        case "2":
                            System.out.println("Редактирование товара");
                            int id = scanner.nextInt();
                            if (id < i) {
                                System.out.println(videoCards[i]);
                                String vib = scanner.next();
                                videoCards[id] = new vidCard(inputZnak("Введите производителя"), inputZnak("Введите наименование"), Integer.valueOf(inputZnak("Введите объем видеопамяти")), Float.valueOf(inputZnak("Введите частоту")), Double.valueOf(inputZnak("Введите цену")));
                                while (!vib.equals("=")) {
                                    switch (vib) {
                                        case "1":
                                            videoCards[id].setproizvoditel(inputZnak("Редактирование производителя"));
                                            break;
                                        case "2":
                                            videoCards[id].setname(inputZnak("Редактирование названия"));
                                            break;
                                        case "3":
                                            videoCards[id].setvideoMemory(Integer.valueOf(inputZnak("Редактирование объема видеопамяти")));
                                            break;
                                        case "4":
                                            videoCards[id].setchastota(Float.valueOf(inputZnak("Редактирование частоты")));
                                            break;
                                        case "5":
                                            videoCards[id].setcost(Double.valueOf(inputZnak("Редактирование цены")));
                                            break;
                                    }
                                    vib = scanner.next();
                                }
                            }
                            break;
                        case "3":
                            System.out.println("Просмотр товара");
                            for (int j = 0; j < i; j++) {
                                System.out.println(videoCards[j]);
                            }
                            break;
                        default:
                            break;
                    }

                }else if (vid==2)
                {

                    System.out.println("1 - Добавление товара \n 2 - Редактирование товара \n 3 - Просмотр товара");
                    String input = scanner.next();
                    switch (input) {
                        case "1":
                            System.out.println("Добавление товара");
                            boks[i++] = new Boks(inputZnak("Введите категорию"), inputZnak("Введите страну производителя"), inputZnak("Введите Название"), Integer.valueOf(inputZnak("Введите длительность сеанса")), Double.valueOf(inputZnak("Введите цену")));
                            break;
                        case "2":
                            System.out.println("Редактирование товара");
                            int id = scanner.nextInt();
                            if (id < i) {
                                System.out.println(boks[i]);
                                String vib = scanner.next();
                                boks[id] = new Boks(inputZnak("Введите категорию"), inputZnak("Введите страну производителя"), inputZnak("Введите Название"), Integer.valueOf(inputZnak("Введите длительность сеанса")), Double.valueOf(inputZnak("Введите цену")));
                                while (!vib.equals("=")) {
                                    switch (vib) {
                                        case "1":
                                            boks[id].setCategorya(inputZnak("Редактирование категории товара"));
                                            break;
                                        case "2":
                                            boks[id].setcountry(inputZnak("Редактирование страны производителя товара"));
                                            break;
                                        case "3":
                                            boks[id].setName(inputZnak("Редактирование названия"));
                                            break;
                                        case "4":
                                            boks[id].setDuration(Integer.valueOf(inputZnak("Редактирование длительности сеанса")));
                                            break;
                                        case "5":
                                            boks[id].setcost2(Double.valueOf(inputZnak("Редактирование цены")));
                                            break;
                                    }
                                    vib = scanner.next();
                                }
                            }
                            break;
                        case "3":
                            System.out.println("Просмотр товара");
                            for (int j = 0; j < i; j++) {
                                System.out.println(boks[j]);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        public static String inputZnak(String type){
            Scanner scanner = new Scanner(System.in);
            System.out.println(type);
            return scanner.next();
        }
    }
