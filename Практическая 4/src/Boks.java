public class Boks {
        private String Categorya;
        private int Duration;
        private String Name;
        private double cost2;
        private String country;

        public Boks (String Categorya, String country, String Name, int Duration, double cost2) {
            this.Categorya=Categorya;
            this.country=country;
            this.cost2=cost2;
            this.Name=Name;
            this.Duration=Duration;
        }
        public void setCategorya(String Categorya) {this.Categorya=Categorya;}
        public void setcountry (String country) {this.country=country;}
        public void setName (String Name) {this.Name=Name;}
        public void setDuration (int Duration) {this.Duration=Duration;}
        public void setcost2 (double cost2) {this.cost2=cost2;}

        public String getCategorya() {return Categorya;}
        public int getDuration() {return Duration;}
        public String getName() {return Name;}
        public double getcost2() {return cost2;}
        public String getCountryountry() {return  country;}
        public String toString () {
            return String.format("Категория: %s \n Страна %s \n Цена %d \n Название %f \n Длительность сеанса %s ",Categorya,country,cost2,Name,Duration);
        }
    }
