import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double Itog = 0;
        List<Workers> WorkersList = new ArrayList<>();
        WorkersList.add(new WorkersWithHour("Посудомойкина", "Алёна", "Михайловна", 4500, Itog));
        WorkersList.add(new WorkersWithHour("Полежко", "Антон", "Дмитриевич", 5000, Itog));
        WorkersList.add(new WorkersWithHour("Земсков", "Виталий", "Александрович", 7100, Itog));
        WorkersList.add(new WorkersWithHour("Толкачев", "Даниил", "Алексеевич", 5100, Itog));
        WorkersList.add(new WorkersWithHour("Громов", "Алексей", "Антонович", 2620, Itog));
        WorkersList.add(new WorkersWithHour("Чурчечко", "Азамат", "Исламова", 3700, Itog));
        WorkersList.add(new WorkersWithHour("Абдулаева", "Джамайла", "Ахмедовна", 2340, Itog));
        WorkersList.add(new WorkersWithHour("Живу", "в", "долг", 1520, Itog));

        WorkersList.add(new WorkersWithFix("Абгеев", "Жеж", "Константинович", 55000));
        WorkersList.add(new WorkersWithFix("Шабанов", "Антон", "Алексеевич", 43000));
        WorkersList.add(new WorkersWithFix("Ахматов", "Ахмат", "Ахматов", 55000));
        WorkersList.add(new WorkersWithFix("Игнатьев", "Игнат", "Игнатьев", 22000));
        WorkersList.add(new WorkersWithFix("Цурова", "Ангелина", "Адамовна", 31000));
        WorkersList.add(new WorkersWithFix("Степанова", "Алина", "Олеговна", 49000));
        WorkersList.add(new WorkersWithFix("Островская", "Наталья", "Алексеевна", 65000));
        WorkersList.add(new WorkersWithFix("Горбунов", "Антон", "Дмитриевич", 41000));

        System.out.println("Список:\n1 - Работники с почасовой оплатой;\n2 - Работники с фиксированной оплатой;\n3 - работники по убыванию зарплаты\n");
        char Choice = scanner.next().charAt(0);

        switch (Choice) {
            case '1':
                for (Workers b : WorkersList) {
                    if (b instanceof WorkersWithHour) b.Printer();
                }
                break;
            case '2':
                for (Workers b : WorkersList) {
                    if (b instanceof WorkersWithFix) b.Printer();
                }
                break;
            case '3':
                double temp = 0;
                for (int i = WorkersList.size() - 1; i >= 1; i--) {
                    for (int j = 0; j < WorkersList.size() - 1; j++) {
                        if (WorkersList.get(j).Solo > WorkersList.get(j + 1).Solo) {
                            temp = WorkersList.get(j).Solo;
                            WorkersList.get(j).Solo = WorkersList.get(j + 1).Solo;
                            WorkersList.get(j + 1).Solo = temp;
                        }
                        }
                    }

                for (int k = 0; k < WorkersList.size(); k++)
                    System.out.println(WorkersList.get(k));

            default:
                System.out.println("Вы ввели неверные данные!");
                System.out.println();
        }
    }
}