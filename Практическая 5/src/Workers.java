abstract class Workers { //основной класс, от которого наследуют поля другие классы
    String Surname, Name, Patronymic;
    double Solo;

    public Workers(String Surname, String Name, String Patronymic, double Solo) {
        this.Surname = Surname;
        this.Name = Name;
        this.Patronymic = Patronymic;
        this.Solo = Solo;
    }

    public abstract void Printer(); //абстрактный метод
}

class WorkersWithHour extends Workers { //классm работников с почасовой оплатой
    double Itog;

    public WorkersWithHour(String Surname, String Name, String Patronymic, double Solo, double Itog) {
        super(Surname, Name, Patronymic, Solo);
        this.Itog = 20.8 * 8 * Solo;
    }

    @Override
    public void Printer() {
        System.out.printf("Фамилия: %s\nИмя: %s\nОтчество: %s\nПочасовая оплата: %8.2f руб/час\nСреднемесячная зарплата: %8.2f руб\n", Surname, Name, Patronymic, Solo, Itog);
        System.out.println();
        Itog = 20.8 * 8 * Solo;
    }

    @Override
    public String toString() {
        return String.format("Фамилия: %s\nИмя: %s\nОтчество: %s\nПочасовая оплата: %8.2f руб/час\nСреднемесячная зарплата: %8.2f руб\n", Surname, Name, Patronymic, Solo, Itog);
    }
}

class WorkersWithFix extends Workers { //класс работников с фиксированной оплатой

    public WorkersWithFix(String Surname, String Name, String Patronymic, double Solo) {
        super(Surname, Name, Patronymic, Solo);
    }

    @Override
    public void Printer() {
        System.out.printf("Фамилия: %s\nИмя: %s\nОтчество: %s\nФиксированная оплата: %8.2f руб\n", Surname, Name, Patronymic, Solo);
        System.out.println();
    }

    @Override
    public String toString() {
        return String.format("Фамилия: %s\nИмя: %s\nОтчество: %s\nФиксированная оплата: %8.2f руб\n", Surname, Name, Patronymic, Solo);
    }
}