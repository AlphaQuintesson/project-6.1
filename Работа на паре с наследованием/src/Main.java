import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String []args) {
        Animal anim = new Animal("Животное", 12, "Зелёный");
        anim.printer();
        Turtle cherepaha = new Turtle("Морская черепаха", 240, "Синий",  true);
        cherepaha.printer();
        Humster humster = new Humster( "Хомяк",  6,  "Бурый",  3);
        humster.printer();
        Animal a = new Turtle("Морская черепаха", 240, "Синий",  true);
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("Животное", 12, "Зелёный"));
        animals.add(new Turtle("Морская черепаха", 240, "Синий",  true));
        animals.add(new Humster( "Хомяк",  6,  "Бурый",  3));
        for(Animal b:animals){
            if(b instanceof Turtle){
                System.out.println("Класс Turtle");
            }
            else if(b instanceof Humster){
                System.out.println("Класс Humster");
            }
            else{
                System.out.println("Класс Animal");
            }
            b.printer();
        }
    }
}
