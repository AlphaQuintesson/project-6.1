public class Turtle extends Animal {
    boolean LiveInWater;
    public Turtle(String Name,int age,String color, boolean LiveInWater){
        super(Name,age,color);
        this.LiveInWater = LiveInWater;
    }

    @Override
    public void printer() {
        System.out.printf("Name: %s\n Age: %d\n Color: %s\n LiveInWater: %b\n", Name,age,color,LiveInWater);
    }
}
